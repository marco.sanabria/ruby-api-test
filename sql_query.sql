-- Select all product names for laptops from table products with average price < $2000

SELECT p.name, p.price
FROM products p
WHERE p.type = 'laptop'
GROUP BY p.name
HAVING AVG(p.price) < 2000;
