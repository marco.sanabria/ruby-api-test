require 'api_client'

module Giphy

  class Base < ApiClient::Base
    always do
      endpoint    'https://api.giphy.com'
    end
  end

  class Gif < Base
    disable_warnings
    def self.search(search_term, limit: 5, offset: 0)
      params(api_key: ENV['API_KEY'], q: search_term, limit: limit, offset: offset).fetch('/v1/gifs/search')
    end
  end
end
