require 'spec_helper'

describe 'API Test' do

  context 'testing the search endpoint' do
    context 'limit 5 and offset 0' do

      let(:response) { Giphy::Gif.search('cheeseburgers') }

      it 'should return 200 OK status code' do
        expect(response).not_to be_empty
        expect(response.meta.status).to eq(200)
        expect(response.meta.msg).to eq('OK')
      end

      it 'should match the correct json schema for gifs' do
        schema = JSON.parse(File.read("#{File.expand_path(File.dirname(File.dirname(__FILE__)))}/support/json-schemas/gif-schema.json"))
        expect(JSON::Validator.validate(schema, response)).to be true
      end

      it 'should validate the response is not empty' do
        expect(response.pagination).not_to be_empty
        expect(response.pagination.total_count).to be > 0
        expect(response.pagination['count']).to eq(5)
        expect(response.pagination['offset']).to eq(0)
        expect(response.data).not_to be_empty
        expect(response.data).to be_an_instance_of(Hashie::Array)
      end
    end

    context 'limit 10 and offset 10' do

      let(:response) { Giphy::Gif.search('cheeseburgers', limit: 10, offset: 10) }

      it 'should validate the response with limit 10 and offset 10' do
        expect(response.pagination).not_to be_empty
        expect(response.pagination.total_count).to be > 0
        expect(response.pagination['count']).to eq(10)
        expect(response.pagination['offset']).to eq(10)
        expect(response.data).not_to be_empty
        expect(response.data).to be_an_instance_of(Hashie::Array)
      end
    end
  end
end
