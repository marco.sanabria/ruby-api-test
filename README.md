# ruby-api-test

## Getting started

- Clone the repository `git clone git@gitlab.com:marco.sanabria/ruby-api-test.git`
- Change directory to repo folder: `cd ruby-api-test`
- Install dependencies: `bundle install`

## Execute tests

### Preconditions
 - You need to create a giphy account and generate an API Key https://developers.giphy.com/

All you need to do in order to run tests is execute: `rspec`


### SQL and Test Scenarios
Part II and III of the test can be found in the sql_query.sql and test_scnearios.txt


# Bonus
## CICD

A simple CICD pipeline was configured on Gitlab CI

![Alt text](./screenshots/Screen\ Shot\ 2022-06-13\ at\ 23.52.45.png?raw=true "Job detail")

![Alt text](./screenshots/Screen\ Shot\ 2022-06-13\ at\ 17.41.56.png?raw=true "Pipelines view")
